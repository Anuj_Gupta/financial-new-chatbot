from rasa_nlu.model import Interpreter

def load_model():
	# where `model_directory points to the folder the model is persisted in`
	interpreter = Interpreter.load('models/current/nlu_model')
	print(interpreter.parse("share price for 5paisa?"))
	
if __name__ == '__main__':
	load_model()
