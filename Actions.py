from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

# from rasa_core.actions.action import Action
from rasa_core_sdk import Action
from rasa_core_sdk.events import SlotSet,AllSlotsReset,UserUtteranceReverted,Restarted
# from rasa_core.events import SlotSet, AllSlotsReset
import requests
import json
from random import randint
import datetime
import os
from nsetools import Nse

class ActionStockPrice(Action):
    def name(self):
        return 'action_stock_price'

    def run(self,dispatcher,tracker,domain):
        try:
            nse = Nse()
            stock_name = tracker.get_slot('stock_name')
            print(stock_name)
            isValid = nse.is_valid_code(stock_name)
            if isValid :
                response = nse.get_quote(stock_name)
                # print(response)
                # StckPrice = response["lastPrice"]
                answer = "Company Name {} \n".format(response["companyName"])
                answer += "Open price is Rs. {}\n".format(str(response["lastPrice"]))                
                answer += "Base price is Rs. {}\n".format(str(response["basePrice"]))
                answer += "Average price  is Rs. {}\n".format(str(response["averagePrice"]))
                answer += "Total Traded Volume  is Rs. {}\n".format(str(response["totalTradedVolume"]))
                answer += "Variable Margin  is Rs. {}\n".format(str(response["varMargin"]))
                answer += "\n\n Would you like to buy this stock?."
                dispatcher.utter_message(answer)
                return [SlotSet('stock_name',stock_name)] 
            else :
                dispatcher.utter_message("kindly enter valid stock")
        except Exception as err:
            print(err)
            dispatcher.utter_message("Unable to process your request")
            return [SlotSet('stock_name','')] 


class ActionGainer(Action):
    def name(self):
        return 'action_stock_gainer'

    def run(self,dispatcher,tracker,domain):
        try:
            # print('Inside get stock')
            gainer = tracker.get_slot('gainer')
            GainerHtml = GetGainer()
            # answer = "Company Name {} \n".format(response["companyName"])
            # answer += "Open price is Rs. {}\n".format(str(response["lastPrice"]))                
            # answer += "Base price is Rs. {}\n".format(str(response["basePrice"]))
            # answer += "Average price  is Rs. {}\n".format(str(response["averagePrice"]))
            # answer += "Total Traded Volume  is Rs. {}\n".format(str(response["totalTradedVolume"]))
            # answer += "Variable Margin  is Rs. {}\n".format(str(response["varMargin"]))
            # answer += "Would you like to buy this stock with 5paisa?."
            dispatcher.utter_message(GainerHtml)
            return [SlotSet('gainer',gainer)] 
        except Exception:
            print(Exception)
            dispatcher.utter_message("Unable to process your request")
            return [SlotSet('gainer',gainer)] 




class ActionLoser(Action):
    def name(self):
        return 'action_stock_loser'

    def run(self,dispatcher,tracker,domain):
        try:
            loser = tracker.get_slot('loser')
            # print('Inside get stock')
            LoserHtml = GetLoser()

            dispatcher.utter_message(LoserHtml)
            return [SlotSet('loser',loser)] 
        except Exception:
            # print(Exception)
            dispatcher.utter_message("Unable to process your request")
            return [SlotSet('loser',loser)] 


class ActionGainerLoser(Action):
    def name(self):
        return 'action_stock_gainer_losers'

    def run(self,dispatcher,tracker,domain):
        try:
            # print('Inside get stock')
            gainer = tracker.get_slot('gainer')
            loser = tracker.get_slot('loser')

            GainerHtml = GetGainer()
            LoserHtml = GetLoser()            

            # answer = "Company Name {} \n".format(response["companyName"])
            # answer += "Open price is Rs. {}\n".format(str(response["lastPrice"]))                
            # answer += "Base price is Rs. {}\n".format(str(response["basePrice"]))
            # answer += "Average price  is Rs. {}\n".format(str(response["averagePrice"]))
            # answer += "Total Traded Volume  is Rs. {}\n".format(str(response["totalTradedVolume"]))
            # answer += "Variable Margin  is Rs. {}\n".format(str(response["varMargin"]))
            # answer += "Would you like to buy this stock with 5paisa?."
            dispatcher.utter_message(GainerHtml)
            dispatcher.utter_message(LoserHtml)
            return [SlotSet('gainer',gainer),SlotSet('loser',loser)] 
        except Exception:
            print(Exception)
            dispatcher.utter_message("Unable to process your request")
            return [SlotSet('gainer',gainer),SlotSet('loser',loser)] 



class ActionSlotReset(Action): 	
    def name(self): 		
        return 'action_slot_reset' 
	
    def run(self, dispatcher, tracker, domain): 		
        return[AllSlotsReset()]		

def GetGainer():
    nse = Nse()
    
    print("Getting Gainer")
    response = nse.get_top_gainers()
    # print(response)
    GainerHtml = "\n ------Top Gainer's------ \n" 
    for GainerVal in response:
        GainerHtml += "\n ------{}------ \n".format(GainerVal["symbol"]) 
        # GainerHtml += "Company Symbol {} \n".format(GainerVal["symbol"])
        GainerHtml += "openPrice Rs {} \n".format(GainerVal["openPrice"]) 
        GainerHtml += "highPrice Rs {} \n".format(GainerVal["highPrice"]) 
        GainerHtml += "lowPrice Rs {} \n".format(GainerVal["lowPrice"]) 
        GainerHtml += "Last trading price Rs {} \n".format(GainerVal["ltp"]) 
        GainerHtml += "previousPrice Rs {} \n".format(GainerVal["previousPrice"]) 
        GainerHtml += "tradedQuantity {} \n".format(GainerVal["tradedQuantity"]) 
        GainerHtml += "-------------- \n\n"
    return GainerHtml

def GetLoser():
    nse = Nse()
    print("Getting Loser")
    response = nse.get_top_losers()
    LoserHtml = "\n ------Top Losers------ \n" 
    for LoserVal in response:
        LoserHtml += "\n ------{}------ \n".format(LoserVal["symbol"]) 
        LoserHtml += "openPrice Rs {} \n".format(LoserVal["openPrice"]) 
        LoserHtml += "highPrice Rs {} \n".format(LoserVal["highPrice"]) 
        LoserHtml += "lowPrice Rs {} \n".format(LoserVal["lowPrice"]) 
        LoserHtml += "Last trading price Rs {} \n".format(LoserVal["ltp"]) 
        LoserHtml += "previousPrice Rs {} \n".format(LoserVal["previousPrice"]) 
        LoserHtml += "tradedQuantity {} \n".format(LoserVal["tradedQuantity"]) 
        LoserHtml += "-------------- \n\n"
    return LoserHtml
        

# def run():
#     nse = Nse()
#     isValid = nse.is_valid_code('5PAISA')
#     print(isValid)
#     if isValid == True:
#         response = nse.get_quote('5PAISA')
#         print(response)
#         print("lastPrice: " + str(response["lastPrice"]))
#     else :
#         print('not valid')


# def run():
#         try:
#             # print('Inside get stock')
#             nse = Nse()
#             response = nse.get_top_gainers()
#             print(response)
#             GainerHtml = ''
#             for GainerVal in response:
#                 GainerHtml += "------{}------ \n".format(GainerVal["symbol"]) 
#                 # GainerHtml += "Company Symbol {} \n".format(GainerVal["symbol"])
#                 GainerHtml += "openPrice Rs {} \n".format(GainerVal["openPrice"]) 
#                 GainerHtml += "highPrice Rs {} \n".format(GainerVal["highPrice"]) 
#                 GainerHtml += "lowPrice Rs {} \n".format(GainerVal["lowPrice"]) 
#                 GainerHtml += "Last trading price Rs {} \n".format(GainerVal["ltp"]) 
#                 GainerHtml += "previousPrice Rs {} \n".format(GainerVal["previousPrice"]) 
#                 GainerHtml += "tradedQuantity {} \n".format(GainerVal["tradedQuantity"]) 
#                 GainerHtml += "-------------- \n\n"
            
#             print(GainerHtml)

#         except Exception:
#             print(Exception)
        

# if __name__ == '__main__':
#     # train("./data/nlu_data.json","./config.yml","./models/nlu")
#     run()