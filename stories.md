## Generated Story 3729856372256153411
* greet
    - utter_greet
* ask_stock_price
    - action_stock_price
* ask_stock_price
    - action_stock_price
* ask_stock_price{"stock_name": "iifl"}
    - slot{"stock_name": "iifl"}
    - action_stock_price
    - slot{"stock_name": "iifl"}
* ask_stock_price{"stock_name": "sbi"}
    - slot{"stock_name": "sbi"}
    - action_stock_price
* ask_stock_price{"stock_name": "sbin"}
    - slot{"stock_name": "sbin"}
    - action_stock_price
    - slot{"stock_name": "sbin"}
* ask_stock_price{"stock_name": "fcl"}
    - slot{"stock_name": "fcl"}
    - action_stock_price
    - slot{"stock_name": "fcl"}
* affirm
    - utter_thanks
    - utter_goodbye
    - action_slot_reset
    - reset_slots
* ask_stock_price{"stock_name": "ekc"}
    - slot{"stock_name": "ekc"}
    - action_stock_price
    - slot{"stock_name": "ekc"}
* greet
    - utter_thanks
    - utter_goodbye
    - action_slot_reset
    - reset_slots
* ask_stock_price{"stock_name": "gss"}
    - slot{"stock_name": "gss"}
    - action_stock_price
    - slot{"stock_name": "gss"}
* ask_stock_price
    - action_stock_price
    - slot{"stock_name": "gss"}
* ask_stock_price{"stock_name": "63moons"}
    - slot{"stock_name": "63moons"}
    - action_stock_price
    - slot{"stock_name": "63moons"}
* ask_stock_price{"stock_name": "adlabs"}
    - slot{"stock_name": "adlabs"}
    - action_stock_price
    - slot{"stock_name": "adlabs"}
* ask_stock_price
    - action_stock_price
    - slot{"stock_name": "adlabs"}
* ask_stock_price
* greet
    - export

