## greet
* greet
    - utter_greet


## goodbye
* goodbye
    - utter_goodbye
    - action_slot_reset


## About bot
* about_bot
    - utter_about_bot


## story_1
* greet
    - utter_greet
* ask_stock_price{"stock_name":"Reliance"}
    - action_stock_price
* goodbye
    - utter_goodbye
    - action_slot_reset

## story_2
* greet 
    - utter_greet
* ask_stock_price {"stock_name": "Infosys Ltd"}
    - action_stock_price
* thanks
    - utter_thanks
    - utter_goodbye
    - action_slot_reset



## story_2_1
* greet 
    - utter_greet
* ask_stock_price {"stock_name": "Infosys Ltd"}
    - action_stock_price
* affirm
    - utter_noted
* thanks
    - utter_thanks
    - utter_goodbye
    - action_slot_reset

## story_3_1
* ask_stock_price {"stock_name": "Reliance"}
    - action_stock_price
* affirm
    - utter_noted
* thanks
    - utter_thanks
    - utter_goodbye
    - action_slot_reset


## story_2_2
* greet 
    - utter_greet
* ask_stock_price {"stock_name": "Infosys Ltd"}
    - action_stock_price
* deny
    - utter_deny
* thanks
    - utter_thanks
    - utter_goodbye
    - action_slot_reset

## story_3_2
* ask_stock_price {"stock_name": "Reliance"}
    - action_stock_price
* deny
    - utter_deny
* thanks
    - utter_thanks
    - utter_goodbye
    - action_slot_reset


## story_2_3
* greet 
    - utter_greet
* ask_stock_price {"stock_name": "MANGLMCEM"}
    - action_stock_price
* deny
    - utter_deny
* thanks
    - utter_thanks
    - utter_goodbye
    - action_slot_reset

## story_3_3
* ask_stock_price {"stock_name": "MAJESCO"}
    - action_stock_price
* deny
    - utter_deny
* thanks
    - utter_thanks
    - utter_goodbye
    - action_slot_reset


## Story_gainer
* ask_gainer {"gainer": "gainer"}
    - action_stock_gainer

## Story_gainer_1
* ask_gainer {"gainer": "gainer"}
    - action_stock_gainer


## Story_loser
* ask_losers {"loser": "losers"}
    - action_stock_loser

## Story_loser_1
* ask_losers {"loser": "losers"}
    - action_stock_loser

## Story_Gainer_loser_1
* ask_gainer+ask_losers {"loser": "losers"}
    - action_stock_gainer_losers

## Story_Gainer_loser_2
* ask_losers+ask_gainer {"gainer": "gainer"} 
    - action_stock_gainer_losers

## Story_Gainer_loser_3
* ask_gainer+ask_losers {"loser": "losers"}
    - action_stock_gainer_losers

## Generated Story 3729856372256153411
* greet
    - utter_greet
* ask_stock_price
    - action_stock_price
* ask_stock_price
    - action_stock_price
* ask_stock_price{"stock_name": "iifl"}
    - slot{"stock_name": "iifl"}
    - action_stock_price
    - slot{"stock_name": "iifl"}
* ask_stock_price{"stock_name": "sbi"}
    - slot{"stock_name": "sbi"}
    - action_stock_price
* ask_stock_price{"stock_name": "sbin"}
    - slot{"stock_name": "sbin"}
    - action_stock_price
    - slot{"stock_name": "sbin"}
* ask_stock_price{"stock_name": "fcl"}
    - slot{"stock_name": "fcl"}
    - action_stock_price
    - slot{"stock_name": "fcl"}
* ask_stock_price{"stock_name": "ekc"}
    - slot{"stock_name": "ekc"}
    - action_stock_price
    - slot{"stock_name": "ekc"}
* greet
    - utter_thanks
    - utter_goodbye
    - action_slot_reset
    - reset_slots
* ask_stock_price{"stock_name": "gss"}
    - slot{"stock_name": "gss"}
    - action_stock_price
    - slot{"stock_name": "gss"}
* ask_stock_price
    - action_stock_price
    - slot{"stock_name": "gss"}
* ask_stock_price{"stock_name": "63moons"}
    - slot{"stock_name": "63moons"}
    - action_stock_price
    - slot{"stock_name": "63moons"}
* ask_stock_price{"stock_name": "adlabs"}
    - slot{"stock_name": "adlabs"}
    - action_stock_price
    - slot{"stock_name": "adlabs"}
* ask_stock_price
    - action_stock_price
    - slot{"stock_name": "adlabs"}


