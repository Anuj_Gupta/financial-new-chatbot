## intent: greet
- Hi
- hey
- heya
- Hello
- What's up
- Heya
- Greetings
- Good  morning
- Good afternoon
- Good evening
- Hey sir
- Hi person
- Hey robot
- Hello bot

##intent: about_bot
 - Tell me [about](info_bot) you?
 - [who are you](info_bot)?
 - what you [can do](info_bot)?
 - [who r u](info_bot)?
 - what [can you do](info_bot)


## intent: goodbye
- Bye
- Goodbye
- Talk to you later
- See you
- See you later
- Bye bye
- Bye for now
- Goodbye bot
 
 

## intent: affirm
- yes
- yup
- yes, that sounds good
- sure
- definitely
- absolutely
- please do
- yes, please
- yes for sure

## intent: thanks
- thanks
- thank you
- Thanks a lot
- Thanks a bunch
- Thank you very much
- Thank you so much


## intent: ask_stock_price
- Current share price of [Reliance](stock_name)
- [Bajaj](stock_name) current open price?
- [5paisa](stock_name) stock price
- should i buy [iifl](stock_name) share price
- should i buy [ONGC](stock_name) ?
- [Infosys](stock_name) Ltd stock rate?
- Buy me [5paisa](stock_name) stock.
- [Infy](stock_name) price
- open price of [Reliance](stock_name) ?
- open price of [5paisa](stock_name) ?
- open price of [Infosys](stock_name) ?
- open price of [iifl](stock_name) ?
- [63moons](stock_name) stock rate ?
- Share rate of [AGLSL](stock_name)
- Is it good idea to buy [INDIGO](stock_name)?
- tell me about [MGL](stock_name)?

## intent: deny
- No
- Nope
- Don't think so
- Nooooo
- No thanks
- No, I don't

## intent: ask_gainer
- Top [gainer](gainer)
- Top 10 [gainer](gainer)
- 10 Top [gainer](gainer)
- show me top [gainer](gainer)
- market [gainer](gainer)
- market [movers](gainer)
- trending stock [gainer](gainer)

## intent: ask_losers
- Top [losers](loser)
- Top 10 [losers](loser)
- 10 Top [losers](loser)
- show me top [losers](loser)
- market [losers](loser)
- trending stock [losers](loser)
- [Loosers](loser)

## intent: ask_gainer+ask_losers
- [gainer](gainer) and [losers](loser)
- Top [losers](loser) and [gainer](gainer)
- Top 10 [gainer](gainer) and market [losers](loser)
- show me top [gainer](gainer) and  [losers](loser)
- show me top [gainer](gainer) and top [losers](loser)
