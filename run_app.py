from rasa_core.channels import HttpInputChannel
from rasa_core.channels.facebook import FacebookInput
from rasa_core.agent import Agent
from rasa_core.interpreter import RegexInterpreter
from rasa_core.interpreter import RasaNLUInterpreter
from rasa_slack_connector import SlackInput

#load your agent

nlu_interpreter = RasaNLUInterpreter('./models/current/nlu_model')
agent = Agent.load("./models/current/dialogue", interpreter=nlu_interpreter)


input_channel = SlackInput(
    'xoxp-423345855715-422905553185-423622735301-260b1770e2ffa01a53c17c92017340af', #slack_dev_token
    'xoxb-423345855715-423622736245-9NYKHWNJPLrmYwMD7vDwle7c', #slack_client_token
    'JaIIDSJS8LVZG3v2Ua8lEIre', #verification_token for interactive messages, events
	True)
	
	
agent.handle_channel(HttpInputChannel(5001, "/", input_channel))